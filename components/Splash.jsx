import Image from 'next/image'

// on kyllä tämäki yks, width height on pakko olla Image komponentissa.
// asetetaan ne 0 ja ylikirjotetaan suoraan 100%. Imagelle annettu luokan nimi ei toimi
export default function Splash() {
    return <header className="header">
    <div className='splash-logo'>
        <Image src={"logo.svg"} alt="Logo" width={0} height={0} style={{
            width: "100%",
            height: "100%",
        }} />
    </div>
    </header>
}